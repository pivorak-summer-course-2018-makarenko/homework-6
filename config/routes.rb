Rails.application.routes.draw do
  root to: 'profile#dashboard'
  devise_for :users,:controllers => {:registrations => "registrations"}
  resources :transactions
  resources :accounts
  resources :users

  namespace :api do
    resources :users, only: [:index, :show, :create, :update]
  end

  get '/profile/info' => 'profile#info'

  get '/profile/:account_id/deposit' => 'profile#deposit', as: :deposit
  post '/profile/:account_id/do_deposit' => 'profile#do_deposit', as: :do_deposit

  get '/profile/:account_id/withdraw' => 'profile#withdraw', as: :withdraw
  post '/profile/:account_id/do_withdraw' => 'profile#do_withdraw', as: :do_withdraw


end
