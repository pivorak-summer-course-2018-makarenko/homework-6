class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :accounts
  #after_create :create_default_account


  def create_default_account
    Account.create(user_id: current_user.id, amount: 0, currency: "USD")
    Account.create(user_id: current_user.id, amount: 0, currency: "UAH")
  end
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
