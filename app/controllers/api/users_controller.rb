module Api
  class UsersController < BaseController
    def index
      render json: users
    end

    def show
      render json: user.to_json(only: [:id,:name])
    end

    def create
      user = User.new(user_params)
        if user.save
          render json: user.to_json(only: [:id, :name]), status: :created
        else
          render json: user.errors, status: :unprocessable_entity
        end
    end

    def update
      if user.update(user_params)
        render json: user.to_json(only: [:id, :name])
      else
        render json: user.errors, status: :unprocessable_entity
      end
    end

    private

    def user
      @user ||= User.find(params[:id])
    end

    def users
      @users ||= User.all
    end

    def user_params
      params.require(:user).permit(:name, :password, :email)
    end
  end
end
